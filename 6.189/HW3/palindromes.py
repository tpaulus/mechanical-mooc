# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 30 2013
# palindromes.py


def is_palindrome(string):
    string = str(string).lower().replace(' ', '')
    return isPal(string)


def isPal(string):
    if len(string) <= 1:
        return True
    else:
        return string[0] == string[-1] and isPal(string[1:-1])


if __name__ == '__main__':
    print is_palindrome('able was i ere i saw elba')
    print is_palindrome('yummy')
