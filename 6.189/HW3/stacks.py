# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 30 2013
# stacks.py


class Stack(object):
    def __init__(self):
        self.list = []

    def push(self, value):
        self.list.insert(0, value)

    def pop(self):
        if len(self.list) == 0:
            return 'The stack is empty.'
        else:
            return self.list.pop(0)


if __name__ == '__main__':
    stack = Stack()
    stack.push(5)
    stack.push(6)
    print stack.pop()
    stack.push(7)
    print stack.pop()
    print stack.pop()
    print stack.pop()