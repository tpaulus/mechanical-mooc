# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 28 2013
# hw3.py

##### Template for Homework 3, exercises 3.1 - 3.5 ######

# **********  Exercise 3.1 ********** 


def list_intersection(l1, l2):
    result = []
    for a in l1:
        for b in l2:
            if a == b:
                if not result.count(a) > 0:
                    result.append(a)
    return result


print 'Exercise 3.1'
print list_intersection([1, 3, 5], [5, 3, 1])
print list_intersection([1, 3, 6, 9], [10, 14, 3, 72, 9])
print list_intersection([2, 3], [3, 3, 3, 2, 10])
print list_intersection([2, 4, 6], [1, 3, 5])

# **********  Exercise 3.2 **********


def ball_collide(ball1, ball2):
    # (x, y, radius)
    x1 = ball1[0]
    y1 = ball1[1]
    x2 = ball2[0]
    y2 = ball2[1]
    x = (x2 - x1)
    y = (y2 - y1)
    d = ((x ** 2) + (y ** 2)) ** .5
    r_sum = ball1[2] + ball2[2]
    if d > r_sum:
        return False
    else:
        return True


print '\nExercise 3.2'
print ball_collide((0, 0, 1), (3, 3, 1))  # Should be False
print ball_collide((5, 5, 2), (2, 8, 3))  # Should be True
print ball_collide((7, 8, 2), (4, 4, 3))  # Should be True

# **********  Exercise 3.3 **********

my_classes = {"6.042": "Math for Computer Science",
              "7.03": "Genetics",
              "5.12": "Organic Chemistry",
              "STS.043": "Science, Technology and Memoir",
              "21W.778": "Science Journalism"}


def add_class(class_num, desc):
    my_classes[class_num] = desc


add_class('21W.735', 'Writing and Reading the Essay')
add_class('4.440', 'Basic Structural Design')
add_class('4.431', 'Architectural Acoustics')
add_class('4.502', 'Design Scripting')
add_class('4.292', 'Special Problems in Architecture Studies')
add_class('6.189', 'Introduction to Python')


def print_classes(course):
    response = False
    for c in my_classes:
        if c.startswith(str(course)):
            response = True
            print c + ' - ' + my_classes[c]
    if not response:
        print "No Course " + course + " classes were taken"


print '\nExercise 3.3'
print_classes('6')
print ''
print_classes('4')
print ''
print_classes('9')

# **********  Exercise 3.4 **********


NAMES = ['Alice', 'Bob', 'Cathy', 'Dan', 'Ed', 'Frank',
         'Gary', 'Helen', 'Irene', 'Jack', 'Kelly', 'Larry']
AGES = [20, 21, 18, 18, 19, 20, 20, 19, 19, 19, 22, 19]


def combine_lists(l1, l2):
    comb_dict = {}
    for i in range(0, len(l1)):
        comb_dict[l1[i]] = l2[i]
    return comb_dict


def people(age):
    result = []
    combined_dict = combine_lists(NAMES, AGES)
    group = combined_dict.keys()
    for person in group:
        if combined_dict[person] == age:
            result.append(person)
    return result


print '\nExercise 3.4 (all should be True)'
print 'Dan' in people(18) and 'Cathy' in people(18)
print 'Ed' in people(19) and 'Helen' in people(19) and 'Irene' in people(19) and 'Jack' in people(
    19) and 'Larry' in people(19)
print 'Alice' in people(20) and 'Frank' in people(20) and 'Gary' in people(20)
print people(21) == ['Bob']
print people(22) == ['Kelly']
print people(23) == []

# **********  Exercise 3.5 **********


def zellers(month, day, year):
    """
    convert numbers to months, and months to numbers
    :param month : month for the requested calculation
    :param day: day for the requested calculation
    :param year: year for the requested calculation
    :return: The day of the week the input date was

    """
    mo_co = {'january': 1,
             'february': 2,
             'march': 3,
             'april': 4,
             'may': 5,
             'june': 6,
             'july': 7,
             'august': 8,
             'september': 9,
             'october': 10,
             'november': 11,
             'december': 12,
             1: 'January',
             2: 'February',
             3: 'March',
             4: 'April',
             5: 'May',
             6: 'June',
             7: 'July',
             8: 'August',
             9: 'September',
             10: 'October',
             11: 'November',
             12: 'December'}

    # convert the weekday number to the actual week day
    wd_co = {0: 'Sunday',
             1: 'Monday',
             2: 'Tuesday',
             3: 'Wednesday',
             4: 'Thursday',
             5: 'Friday',
             6: 'Saturday'}
    month = int(mo_co.get(month.lower(), month))
    day = int(day)
    year = int(year)

    monthC = month      # Save the inputted, non-converted month
    # convert the date to a March is the first month system
    if monthC < 3:
        monthC += 10
        year -= 1
    else:
        monthC -= 2

    if day <= 0 or day > 31:
        quit('There is no day ' + str(day) + '!')

    century = int(year / 100)
    year -= (century * 100)


    # Components of the Zeller Algorithm
    w = ((13.0 * monthC - 1) / 5.0)
    x = year / 4
    y = century / 4
    z = w + x + y + day + year - 2 * century
    r = z % 7
    wd = int(r + .5)       # Round the float by adding .5 an then truncating

    if wd < 0:      # Ensure that the value is positive
        wd += 7

    wd = wd_co[wd]  # Convert the number to a day
    return wd


print '\nExercise 3.5'
print zellers("March", 10, 1940) == "Sunday" # This should be True