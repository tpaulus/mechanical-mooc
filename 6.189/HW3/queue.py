# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 30 2013
# queue.py


class Queue(object):
    def __init__(self):
        self.list = []

    def insert(self, value):
        self.list.append(value)

    def remove(self):
        if len(self.list) == 0:
            return 'The Queue is empty.'
        else:
            return self.list.pop(0)


if __name__ == '__main__':
    queue = Queue()
    queue.insert(5)
    queue.insert(6)
    print queue.remove()
    queue.insert(7)
    print queue.remove()
    print queue.remove()
    print queue.remove()