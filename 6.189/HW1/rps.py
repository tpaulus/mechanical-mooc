# Name: Tom Paulus
# Section: MIT-OCW
# Date: June 18 2013
# rps.py

p1w = False     # Has Player 1 Won?
p2w = False     # Has Player 2 Won?
tie = False     # Is it a Tie?

p1 = raw_input("Player 1? ")
p2 = raw_input("Player 2? ")

# The next lines check if it is a valid input,
# also this program tolerated capitalization problems,
# by making all inputs lower case
if p1.lower() != 'rock' and 'paper' and 'scissors':
    quit(p1 + ' is not a valid input.')
else:
    p1 = p1.lower()
if p2.lower() != 'rock' and 'paper' and 'scissors':
    quit(p2 + ' is not a valid input.')
else:
    p2 = p2.lower()

#Compare Inputs to find winner
if p1 == 'rock':
    if p2 == 'rock':
        tie = True
    if p2 == 'scissors':
        p1w = True
    if p2 == 'paper':
        p2w = True

elif p1 == 'scissors':
    if p2 == 'rock':
        p2w = True
    if p2 == 'scissors':
        tie = True
    if p2 == 'paper':
        p1w = True

elif p1 == 'paper':
    if p2 == 'rock':
        p2w = True
    if p2 == 'scissors':
        p1w = True
    if p2 == 'paper':
        tie = True

# Print Out the Results
if p1w:
    print 'Player 1 Wins.'
if p2w:
    print 'Player 2 Wins.'
if tie:
    print 'It was a tie.'