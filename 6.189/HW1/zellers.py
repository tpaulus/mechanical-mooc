# Name: Tom Paulus
# Section: MIT-OCW
# Date: June 20 2013
# zellers.py

import datetime

month = None
day = None
year = None

# convert numbers to months, and months to numbers
mo_co = {'january': 1,
         'february': 2,
         'march': 3,
         'april': 4,
         'may': 5,
         'june': 6,
         'july': 7,
         'august': 8,
         'september': 9,
         'october': 10,
         'november': 11,
         'december': 12,
         1: 'January',
         2: 'February',
         3: 'March',
         4: 'April',
         5: 'May',
         6: 'June',
         7: 'July',
         8: 'August',
         9: 'September',
         10: 'October',
         11: 'November',
         12: 'December'}

# convert the weekday number to the actual week day
wd_co = {0: 'Sunday',
         1: 'Monday',
         2: 'Tuesday',
         3: 'Wednesday',
         4: 'Thursday',
         5: 'Friday',
         6: 'Saturday'}
# Ensure that a Month, Day, and Year are entered
while True:
    month = raw_input('Month? ')
    try:
        month = int(mo_co.get(month.lower(), month))    # convert the month to a number
    except ValueError:
        print month + ' is not a valid month,  Please enter a valid month.'          # Catch if the month is not valid
        month = ''
    if month != '':
        if month <= 0 or month > 12:        # Check that the month is valid
            print 'There is no month ' + str(month) + '!, Please enter a valid month.'
        else:
            break
while True:
    try:
        day = int(raw_input('Day? '))
        break
    except ValueError:
        print 'Please enter a Day.'
while True:
    try:
        year = int(raw_input('Year? '))
        break
    except ValueError:
        print 'Please enter a Year.'

date = datetime.date(year, month, day)

yearO = year        # Save the original year
monthC = month      # Save the inputted, non-converted month
# convert the date to a March is the first month system
if monthC < 3:
    monthC += 10
    year -= 1
else:
    monthC -= 2

if day <= 0 or day > 31:
    quit('There is no day ' + str(day) + '!')

century = int(year / 100)
year -= (century * 100)


# Components of the Zeller Algorithm
w = ((13.0 * monthC - 1) / 5.0)
x = year / 4
y = century / 4
z = w + x + y + day + year - 2 * century
r = z % 7
wd = int(r + .5)       # Round the float by adding .5 an then truncating

if wd < 0:      # Ensure that the value is positive
    wd += 7

wd = wd_co[wd]  # Convert the number to a day


#Compare today to the inputted date determine if it was, is, or will be
if date < datetime.date.today():
    print '\t' + mo_co[month], str(day) + ',', yearO, 'was a', wd   # Print out the result

if date == datetime.date.today():
    print '\t' + mo_co[month], str(day) + ',', yearO, 'is a', wd   # Print out the result

if date > datetime.date.today():
    print '\t' + mo_co[month], str(day) + ',', yearO, 'will be a', wd   # Print out the result