# Name: Tom Paulus
# Section: MIT-OCW
# Date: June 17 2013
# hw1.py

##### Template for Homework 1, exercises 1.2-1.5 ######

print "********** Exercise 1.2 **********"

print '  |  |  \n--------\n  |  |  \n--------\n  |  |  \n'

print "********** Exercise 1.3 **********"

g1 = '  |  |  \n'
g2 = '--------\n'

print g1 + g2 + g1 + g2 + g1

print "********** Exercise 1.4 **********"
print "********* Part II *************"

a = (3 * 5) / (2 + 3)
b = ((7 + 9) ** (1 / 2.0)) * 2
c = (4 - 7) ** 3
d = (-19 + 100) ** (1 / 4.0)
e = 6 % 4

print a
print b
print c
print d
print e

print "********* Part III *************"

f = 9 * (100 + 25) ** (1 / 3.0)
g = (9 * (100 + 25)) ** (1 / 3.0)

print f
print g

print "********** Exercise 1.5 **********"

first_name = raw_input('Enter your first name: ')
last_name = raw_input('Enetr your last name: ')
print "Enter your date of birth:"
month = raw_input('Month? ')
day = raw_input('Day? ')
year = raw_input('Year? ')

print first_name, last_name, 'was born on', month, day + ',', year