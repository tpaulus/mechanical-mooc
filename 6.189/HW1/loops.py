# Name: Tom Paulus
# Section: MIT-OCW
# Date: June 18 2013
# loops.py

#1
for x in range(2, 11):
    print (1.0 / x)

#2
x = abs(int(raw_input('Starting Number ')))
while x > 0:
    x -= 1
    print x

#3
base = int(raw_input("Base: "))
exp = int(raw_input("Exponent: "))
result = 1

for x in range(0, exp):
    result *= base

print result

#4
while True:
    num = int(raw_input("Enter a Number Divisible by 2: "))
    if (num % 2) == 0:
        break
    else:
        print "Hey little Punk, that number ain't' divisible by 2! Try Again, Punk..."
print 'Yay, you can do Second Grade Math!! Congrats!'