# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 1 2013
# hw2.py

import math

# **********  Exercise 2.0 **********
import random


def f1(x):
    print x + 1


def f2(x):
    return x + 1

# **********  Exercise 2.1 ********** 


def rps(p1, p2):
    """
    :param p1: Player 1's choice
    :param p2: Player 2's Choice
    :return: Who won
    """
    p1w = False
    p2w = False
    tie = False
    if p1 == 'rock':
        if p2 == 'rock':
            tie = True
        if p2 == 'scissors':
            p1w = True
        if p2 == 'paper':
            p2w = True

    elif p1 == 'scissors':
        if p2 == 'rock':
            p2w = True
        if p2 == 'scissors':
            tie = True
        if p2 == 'paper':
            p1w = True

    elif p1 == 'paper':
        if p2 == 'rock':
            p2w = True
        if p2 == 'scissors':
            p1w = True
        if p2 == 'paper':
            tie = True

    # Print Out the Results
    if p1w:
        return True, False
    if p2w:
        return False, True
    if tie:
        return False, False


print '\tExercise 2.1'
print rps('rock', 'rock')
print rps('paper', 'scissors')
print rps('scissors', 'rock')

# ********** Exercise 2.2 ********** 


def is_divisible(m, n):
    """
    :param m: Original Number
    :param n: number to be divided by
    :return: If m is divisible by n

    """
    if n == 0:
        print 'Cannot divide by zero!'
        return False
    if m % n == 0:
        return True
    else:
        return False


print '\n\t Exercise 2.2 A'
print is_divisible(10, 5)  # This should return True
print is_divisible(18, 7)  # This should return False
print is_divisible(42, 0)  # What should this return?


def not_equal(m, n):
    if m == n:
        return False
    else:
        return True


print '\n\t Exercise 2.2 B'
print not_equal(0, 0)
print not_equal(4, 8)
print not_equal(4.225, 4)

# ********** Exercise 2.3 ********** 


def multadd(a, b, c):
    return a * b + c


a1 = math.cos(math.pi / 4)
a2 = 1 / 2.0
a3 = math.sin(math.pi / 4)

b1 = 2.0
b2 = math.log(12, 7)
b3 = math.ceil(276 / 19)

print '\n\t Exercise 2.3 A'
angle_test = multadd(a1, a2, a3)
print "sin(pi/4) + cos(pi/4)/2 is:", angle_test

print '\n\t Exercise 2.3 B'
ceiling_test = multadd(b1, b2, b3)
print "ceiling(276/19) + 2 log_7(12) is:", ceiling_test


def yikes(x):
    a = math.e ** -x
    return multadd(x, a, math.sqrt(1 - a))


print '\n\t Exercise 2.3 C'
x = 5
print "yikes(5) =", yikes(x)

# ********** Exercise 2.4 **********


def rand_divis_3():
    lo = 0
    hi = 100
    if random.randint(lo, hi) % 3 == 0:
        return True
    else:
        return False


print '\n\t Exercise 2.4 A'
print rand_divis_3()
print rand_divis_3()
print rand_divis_3()


def roll_dice(sides, number):
    """remember that a die's lowest number is 1; its highest is the number of sides it has"""
    for r in range(0, number):
        print random.randint(1, sides)
    return "That's All"


print '\n\t Exercise 2.4 B'
print roll_dice(2, 5)
print roll_dice(6, 3)
print roll_dice(22, 2)

# ********** Exercise 2.5 **********


def roots(a, b, c):
    d = b ** 2 - (4 * a * c)
    if d == 0:
        ans = (-1 * b) / (2.0 * a)
        return ans
    if d > 0:
        ans1 = (-1 * b + math.sqrt(d)) / (2.0 * a)
        ans2 = (-1 * b - math.sqrt(d)) / (2.0 * a)
        return ans1, ans2
    if d < 0:
        #Imaginary!!
        ans1 = complex((-1 * b), math.sqrt(d * -1)) / (2.0 * a)
        ans2 = complex((-1 * b), -1 * math.sqrt(d * -1)) / (2.0 * a)
        return ans1, ans2


print '\n\t Exercise 2.5'
print roots(1, 0, -25)
print roots(7, 5, -14)
print roots(3, 9, 7)
print roots(1, 0, 22)
