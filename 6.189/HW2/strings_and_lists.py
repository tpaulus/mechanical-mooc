# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 10 2013
# strings_and_lists.py

# **********  Exercise 2.7 **********


def sum_all(number_list):
    """
    :param number_list: a list of numbers
    :return: Sum of all the numbers in the number_list
    """
    total = 0
    for num in number_list:
        total += num

    return total

# Test cases
print "sum_all of [4, 3, 6] is:", sum_all([4, 3, 6])
print "sum_all of [1, 2, 3, 4] is:", sum_all([1, 2, 3, 4])


def cumulative_sum(number_list):
    """
    :param number_list: a list of numbers
    :return: a list of with the cumulative sum
    """
    cum_sum = []
    total = 0
    for num in number_list:
        total += num
        cum_sum.append(total)

    return cum_sum

# Test Cases
print "cumulative_sum of [4, 3, 6] is:", cumulative_sum([4, 3, 6])
print "cumulative_sum of [1, 2, 3, 4] is:", cumulative_sum([1, 2, 3, 4])
print

# **********  Exercise 2.8 **********


def report_card():
    """
    Create a report card for the user based on his input
    """
    class_index = {}
    grade_Average = []
    num_classes = int(raw_input("How many classes did you take? "))
    for c in range(0, num_classes):
        class_name = raw_input("What was the name of this class? ")
        class_grade = float(raw_input("What was your grade? "))
        class_index[class_name] = class_grade

    print "...\nREPORT CARD:"
    for c in range(0, num_classes):
        print class_index.keys()[c], '-', class_index[class_index.keys()[c]]
    for c in range(0, num_classes):
        grade_Average.append(class_index[class_index.keys()[c]])
    print 'Overall GPA', sum(grade_Average) / float(num_classes)


# Test Cases
report_card()
print
##  Result
# REPORT CARD:
# English 2 H - 92.0
# Photography - 100.0
# Spanish 2 - 93.0
# Pre Calc H- 81.4
# AP European History - 99.0
# Medical Chemistry - 99.0
# Overall GPA 94.0666666667


# **********  Exercise 2.9 **********


VOWELS = ['a', 'e', 'i', 'o', 'u']


def pig_latin(original):
    """
    :param original: string to convert to pig-latin
    :return: Converted String
    """

    if len(original) > 0 and original.isalpha():
        word = original.lower()
        first = word[0]
        if VOWELS.count(first):
            return word + 'hay'

        else:
            return word[1:] + word[:1] + 'ay'


# Test Cases
print pig_latin(raw_input('Enter a word to be converted: '))
print


# **********  Exercise 2.10 **********

a = [x ** 3 for x in range(1, 11)]
print a


b = [x + y for x in ['h', 't'] for y in ['h', 't']]
print b


def c(string):
    """
    :param string: Input text which will be scanned for vowels
    :return: A list comprehension with all the vowels of the input string
    """
    vowels = [letter for letter in string.lower() if VOWELS.count(letter)]
    return vowels


print c(raw_input('Enter Sting for 2.10 part 3: '))

d1 = [x + y for x in [10, 20, 30] for y in [1, 2, 3]]
print d1

f = [10, 20, 30]
g = [1, 2, 3]
ans = []
for x in f:
    for y in g:
        ans.append(x + y)

print ans
