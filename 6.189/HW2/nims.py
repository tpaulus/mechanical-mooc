# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 1 2013
# nims.py


def play_nims(pile, max_stones):
    """
    An interactive two-person game; also known as Stones.
    @param pile: the number of stones in the pile to start
    @param max_stones: the maximum number of stones you can take on one turn
    """

    ## Basic structure of program (feel free to alter as you please):
    print 'There are', pile, 'stones on the pile.'

    while pile > 0:
        while True:
            p1 = int(raw_input('Player 1? '))
            if pile - p1 >= 0:
                if 1 <= p1 <= max_stones:
                    pile -= p1
                    break
                else:
                    print 'You cannot remove', p1, 'stones. Try Again...'
            else:
                print 'You cannot remove', p1, 'stones. Try Again...'
        if pile == 0:
            print 'There are 0 stones left, Player 1 Won!'
            break
        while True:
            p2 = int(raw_input('Player 2? '))
            if pile - p2 >= 0:
                if 1 <= p2 <= max_stones:
                    pile -= p2
                    break
                else:
                    print 'You cannot remove', p2, 'stones. Try Again...'
            else:
                print 'You cannot remove', p2, 'stones. Try Again...'
        if pile == 0:
            print 'There are 0 stones left, Player 2 Won!'
            break
        else:
            print 'There are', pile, 'stones remaining on the pile.'

    print "Game over"


play_nims(100, 5)