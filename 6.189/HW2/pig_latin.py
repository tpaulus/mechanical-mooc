# Name: Tom Paulus
# Section: MIT-OCW
# Date: July 11 2013
# pig_latin.py

VOWELS = ['a', 'e', 'i', 'o', 'u']
BEGINNING = ['th', 'st', 'qu', 'pl', 'tr']
PUNCTUATION = [',', '.', '!', ':', ';']
QUIT = 'QUIT'


def convert(original):
    """
    :param original: The input word that is to be converted
    :return: the converted word
    """
    result = ''
    word = original.lower()
    first = word[0]
    start = word[:2]
    stem = word[2:]

    if BEGINNING.count(start):
        result = stem + start + 'ay'

    elif VOWELS.count(first):
        result = word + 'hay'

    else:
        result = word[1:] + word[:1] + 'ay'

    return punctuation(result)


def punctuation(word):
    """
    :param word: Input word, which need punctuation moved to the end
    :return: fixed word
    """
    for mark in PUNCTUATION:
        if word.count(mark):
            word = word.replace(mark, '') + mark
            break
    return word

if __name__ == '__main__':
    while True:
        result = ''
        original = raw_input('Enter Phrase(s) to be converted: ')
        if original.strip() == QUIT:
            break
        words = original.lower().split()
        for word in words:
            result = "{0} {1}".format(result, convert(word))
        print result.strip()