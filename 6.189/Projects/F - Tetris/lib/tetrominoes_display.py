# Name: Tom Paulus
# Section: MIT-OCW
# Date: August 7 2013
# tetrominoes.py

from graphics import *
from All_Shapes import *

if __name__ == '__main__':
    win = GraphWin("Tetrominoes", 900, 150)

    tetrominoes = [I_shape, J_shape, L_shape, O_shape, S_shape, T_shape, Z_shape]
    x = 3
    for tetromino in tetrominoes:
        shape = tetromino(Point(x, 1))
        shape.draw(win)
        x += 4

    win.mainloop()