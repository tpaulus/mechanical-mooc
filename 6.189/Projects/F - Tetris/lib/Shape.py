#! /usr/bin/python
#Written By Tom Paulus, @tompaulus, www.tompaulus.com

from Block import Block


class Shape(object):
    """ Shape class:
        Base class for all the tetris shapes
        Attributes:
            :var blocks:  :type: int - the list of blocks making up the shape
            :var rotation_dir:  :type: int - the current rotation direction of the shape
            :var shift_rotation_dir: :type: bool - whether or not the shape rotates
    """

    def __init__(self, coords, color):
        self.blocks = []
        self.rotation_dir = 1
        ### A boolean to indicate if a shape shifts rotation direction or not.
        ### Defaults to false since only 3 shapes shift rotation directions (I, S and Z)
        self.shift_rotation_dir = False

        for pos in coords:
            self.blocks.append(Block(pos, color))

    def get_blocks(self):
        """
        :return: The list of blocks
        :rtype: list
        """

        return self.blocks

    def draw(self, win):
        """  :param win: The window where the drawing should take place
             :type win: CanvasFrame

            Draws the shape:
            i.e. draws each block
        """
        for block in self.blocks:
            block.draw(win)

    def move(self, dx, dy):
        """ :param dx: how far to move in the x direction
            :type dx: int
            :param dy: how far to move in the y direction
            :type dy: int

            moves the shape dx squares in the x direction
            and dy squares in the y direction, i.e.
            moves each of the blocks
        """
        for block in self.blocks:
            block.move(dx, dy)

    def can_move(self, board, dx, dy):
        """
            :param board: The board where the game takes place
            :type board: Board
            :param dx: How far to move in the x direction
            :type dx: int
            :param dy: How fat to move in the y direction
            :type dy: int

            :return:
            :rtype: bool

            checks if the shape can move dx squares in the x direction
            and dy squares in the y direction, i.e.
            check if each of the blocks can move
            Returns True if all of them can, and False otherwise

        """

        for block in self.blocks:
            if not block.can_move(board, dx, dy):
                return False
        return True

    def get_rotation_dir(self):
        """ :return: The current rotation direction
            :rtype: int
        """
        return self.rotation_dir

    def can_rotate(self, board):
        """ :param board: The board where the game takes place
            :type board: Board
            :return: If the shape can rotate
            :rtype: bool


            Checks if the shape can be rotated.

            1. Get the rotation direction using the get_rotation_dir method
            2. Compute the position of each block after rotation and check if
            the new position is valid
            3. If any of the blocks cannot be moved to their new position,
            return False

            otherwise all is good, return True
        """
        for block in self.blocks:
            new_x = self.blocks[1].x - self.rotation_dir * self.blocks[1].y + self.rotation_dir * block.y
            new_y = self.blocks[1].y + self.rotation_dir * self.blocks[1].x - self.rotation_dir * block.x
            dx = new_x - block.x
            dy = new_y - block.y
            if not block.can_move(board, dx, dy):
                return False
        return True

    def rotate(self, board):
        """ :param board: The board where the game takes place
            :type board: Board

            rotates the shape:
            1. Get the rotation direction using the get_rotation_dir method
            2. Compute the position of each block after rotation
            3. Move the block to the new position

        """

        for block in self.blocks:
            new_x = self.blocks[1].x - self.rotation_dir * self.blocks[1].y + self.rotation_dir * block.y
            new_y = self.blocks[1].y + self.rotation_dir * self.blocks[1].x - self.rotation_dir * block.x
            block.move(new_x - block.x, new_y - block.y)

        ### This should be at the END of your rotate code.
        ### DO NOT touch it. Default behavior is that a piece will only shift
        ### rotation direction after a successful rotation. This ensures that
        ### pieces which switch rotations definitely remain within their
        ### accepted rotation positions.
        if self.shift_rotation_dir:
            self.rotation_dir *= -1
