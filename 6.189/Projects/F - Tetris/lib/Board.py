#! /usr/bin/python
#Written By Tom Paulus, @tompaulus, www.tompaulus.com

from graphics import *
from Block import Block


class Board(object):
    """ Board class: it represents the Tetris board

        Attributes: width - type:int - width of the board in squares
                    height - type:int - height of the board in squares
                    canvas - type:CanvasFrame - where the pieces will be drawn
                    grid - type:Dictionary - keeps track of the current state of
                    the board; stores the blocks for a given position
    """

    def __init__(self, win, width, height):
        self.width = width
        self.height = height

        # create a canvas to draw the tetris shapes on
        self.canvas = CanvasFrame(win, self.width * Block.BLOCK_SIZE,
                                  self.height * Block.BLOCK_SIZE)
        self.canvas.setBackground('light gray')
        self.pause_back = None
        self.pause_h1 = None
        self.pause_h2 = None

        self.intro_back = None
        self.intro_h1 = None
        self.intro_h2 = None
        # create an empty dictionary
        # currently we have no shapes on the board
        self.grid = {}

    def draw_shape(self, shape):
        """ :param shape: The Shape to draw
            :type shape: Shape
            :return: If the shape was drawn
            :rtype: bool

            draws the shape on the board if there is space for it
            and returns True, otherwise it returns False
        """
        if shape.can_move(self, 0, 0):
            shape.draw(self.canvas)
            return True
        return False

    def can_move(self, x, y):
        """
            :param x: X coordinate to check
            :type x: int
            :param y: Y coordinate to check
            :type y: int

            :return: If the space is available
            :rtype: bool

            1. check if it is ok to move to square x,y
            if the position is outside of the board boundaries, can't move there
            return False

            2. if there is already a block at that postion, can't move there
            return False

            3. otherwise return True

        """
        inside_bounds = False
        occupied = False

        if 0 <= x <= self.width - 1:
            if 0 <= y <= self.height - 1:
                inside_bounds = True

        if (x, y) in self.grid:
            occupied = True

        if inside_bounds and not occupied:
            return True
        else:
            return False

    def add_shape(self, shape):
        """ :param shape: What shape to add
            :type shape: Shape

            add a shape to the grid, i.e.
            add each block to the grid using its
            (x, y) coordinates as a dictionary key

            Hint: use the get_blocks method on Shape to
            get the list of blocks

        """

        for block in shape.get_blocks():
            x = block.x
            y = block.y
            self.grid[(x, y)] = block


    def delete_row(self, y):
        """ :param y: the y coordinate of the row to be removed
            :type y: int

            remove all the blocks in row y
            to remove a block you must remove it from the grid
            and erase it from the screen.
            If you don't remember how to erase a graphics object
            from the screen, take a look at the Graphics Library
            handout

        """
        for x in range(self.width):
            block = self.grid[(x, y)]
            block.undraw()
            del (self.grid[(x, y)])
        pass

    def is_row_complete(self, y):
        """ :param y: the row which should be checked for completeness
            :type y: int

            :return: If the row is completed
            :rtype: bool

            for each block in row y
            check if there is a block in the grid (use the in operator)
            if there is one square that is not occupied, return False
            otherwise return True

        """

        for x in range(self.width):
            if (x, y) not in self.grid:
                return False
        return True

    def move_down_rows(self, y_start):
        """ :param y_start: The lowest row form which to begin moving the rows above it down toward
            :type y_start: int

            for each row from y_start to the top
                for each column
                    check if there is a block in the grid
                    if there is, remove it from the grid
                    and move the block object down on the screen
                    and then place it back in the grid in the new position
        """
        for y in range(y_start, 0, -1):
            for x in range(0, self.width):
                if (x, y) in self.grid:
                    block = self.grid[(x, y)]
                    del (self.grid[(x, y)])
                    block.move(0, 1)
                    self.grid[(x, y + 1)] = block

    def remove_complete_rows(self):
        """ removes all the complete rows
            1. for each row, y,
            2. check if the row is complete
                if it is,
                    delete the row
                    move all rows down starting at row y - 1

        """

        num_removed = 0
        for y in range(self.height):
            if self.is_row_complete(y):
                self.delete_row(y)
                self.move_down_rows(y - 1)
                num_removed += 1
        return num_removed

    def intro(self, state):
        """ display "Game Paused" message in the center of the board
            HINT: use the Text class from the graphics library
        """

        mid_x = int(self.width / 2)
        mid_y = int(self.height / 2)

        width = 4.5
        height = 2

        tl = Point((mid_x + width) * 30, (mid_y - height) * 30)
        br = Point((mid_x - width) * 30, (mid_y + height) * 30)
        if state:
            self.intro_back = Rectangle(tl, br)
            self.intro_back.setFill('ivory2')
            self.intro_back.setWidth(2)
            self.intro_back.setOutline('ivory1')
            self.intro_h1 = Text(Point(mid_x * 30, mid_y * 30 - 16), 'Welcome to Tetris!')
            self.intro_h1.setSize(30)
            self.intro_h1.setTextColor("navy Blue")
            self.intro_h2 = Text(Point(mid_x * 30, mid_y * 30 + 24), "Press any key to begin...")
            self.intro_h2.setSize(22)
            self.intro_h2.setTextColor("forest green")

            self.intro_back.draw(self.canvas)
            self.intro_h1.draw(self.canvas)
            self.intro_h2.draw(self.canvas)

        else:
            self.intro_back.undraw()
            self.intro_h1.undraw()
            self.intro_h2.undraw()


    def pause(self, state):
        """ display "Game Paused" message in the center of the board
            HINT: use the Text class from the graphics library
        """

        mid_x = int(self.width / 2)
        mid_y = int(self.height / 2)

        width = 4.5
        height = 2

        tl = Point((mid_x + width) * 30, (mid_y - height) * 30)
        br = Point((mid_x - width) * 30, (mid_y + height) * 30)
        if state:
            self.pause_back = Rectangle(tl, br)
            self.pause_back.setFill('ivory2')
            self.pause_back.setWidth(2)
            self.pause_back.setOutline('ivory1')
            self.pause_h1 = Text(Point(mid_x * 30, mid_y * 30 - 16), 'Game Paused')
            self.pause_h1.setSize(36)
            self.pause_h1.setTextColor("red3")
            self.pause_h2 = Text(Point(mid_x * 30, mid_y * 30 + 24), "Press 'p' to resume")
            self.pause_h2.setSize(24)
            self.pause_h2.setTextColor("red3")

            self.pause_back.draw(self.canvas)
            self.pause_h1.draw(self.canvas)
            self.pause_h2.draw(self.canvas)

        else:
            self.pause_back.undraw()
            self.pause_h1.undraw()
            self.pause_h2.undraw()

    def game_over(self):
        """ display "Game Over !!!" message in the center of the board
            HINT: use the Text class from the graphics library
        """

        mid_x = int(self.width / 2)
        mid_y = int(self.height / 2)

        height = -4
        width = 2

        tl = Point((mid_x - height) * 30, (mid_y - width) * 30)
        br = Point((mid_x + height) * 30, (mid_y + width) * 30)

        back = Rectangle(tl, br)
        back.setFill('ivory2')
        back.setWidth(2)
        back.setOutline('ivory1')
        text = Text(Point(mid_x * 30, mid_y * 30), 'Game Over...')
        text.setSize(32)
        text.setTextColor(color_rgb(6, 100, 67))

        back.draw(self.canvas)
        text.draw(self.canvas)