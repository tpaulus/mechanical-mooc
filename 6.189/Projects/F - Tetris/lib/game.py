#! /usr/bin/python
#Written By Tom Paulus, @tompaulus, www.tompaulus.com

import random

from graphics import *
from All_Shapes import *
from Board import *
from ScoreBoard import *


class Tetris(object):
    """ Tetris class: Controls the game play
        Attributes:
            SHAPES - type: list (list of Shape classes)
            DIRECTION - type: dictionary - converts string direction to (dx, dy)
            BOARD_WIDTH - type:int - the width of the board
            BOARD_HEIGHT - type:int - the height of the board
            board - type:Board - the tetris board
            win - type:Window - the window for the tetris game
            delay - type:int - the speed in milliseconds for moving the shapes
            current_shapes - type: Shape - the current moving shape on the board

    """

    SHAPES = [I_shape, J_shape, L_shape, O_shape, S_shape, T_shape, Z_shape]
    DIRECTION = {'Left': (-1, 0), 'Right': (1, 0), 'Down': (0, 1)}
    BOARD_WIDTH = 10
    BOARD_HEIGHT = 20

    def __init__(self, win):
        self.score = 0
        self.board = Board(win, self.BOARD_WIDTH, self.BOARD_HEIGHT)
        self.scoreBoard = ScoreBoard(win, self.BOARD_WIDTH * Block.BLOCK_SIZE, 50, 0)

        self.win = win
        self.delay = 1000  # ms
        self.paused = False
        self.is_active = False

        # sets up the keyboard events
        # when a key is called the method key_pressed will be called
        self.win.bind_all('<Key>', self.key_pressed)

        self.board.intro(not self.is_active)

    def startGame(self):
        self.board.intro(not self.is_active)
        # set the current shape to a random new shape
        self.current_shape = self.create_new_shape()

        # Draw the current_shape oan the board (take a look at the draw_shape method in the Board class)
        self.board.draw_shape(self.current_shape)

        self.animate_shape()

    def create_new_shape(self):
        """
            Create a random new shape that is centered
             at y = 0 and x = int(self.BOARD_WIDTH/2)
            return the shape

            :return: the Shape Object that was created
            :rtype: Shape
        """

        x = int(self.BOARD_WIDTH / 2)
        y = 0
        if self.board.can_move(x, y):
            piece_number = random.randrange(0, len(self.SHAPES) - 1)
            piece = self.SHAPES[piece_number](Point(x, y))
            return piece
        else:
            self.paused = True
            self.board.game_over()
            self.scoreBoard.set_final_score(self.score)

    def animate_shape(self):
        """
            animate the shape - move down at equal intervals
            specified by the delay attribute
        """

        if not self.paused:
            self.do_move('Down')
        self.win.after(self.delay, self.animate_shape)

    def do_move(self, direction):
        """
            :param direction: The string for the keyboard input
            :type direction: str
            :return: If the move occurred
            :rtype: bool

            Move the current shape in the direction specified by the parameter:
            First check if the shape can move. If it can, move it and return True
            Otherwise if the direction we tried to move was 'Down',
            1. add the current shape to the board
            2. remove the completed rows if any 
            3. create a new random shape and set current_shape attribute
            4. If the shape cannot be drawn on the board, display a
               game over message

        """
        dx, dy = self.DIRECTION[direction]
        if self.current_shape.can_move(self.board, dx, dy):
            self.current_shape.move(dx, dy)
            return True
        else:
            if direction == "Down":
                self.board.add_shape(self.current_shape)
                removed = self.board.remove_complete_rows()
                if removed == 4:
                    self.increment_score(32)
                else:
                    self.increment_score(removed ** 2)
                self.current_shape = self.create_new_shape()
                if not self.board.draw_shape(self.current_shape):
                    self.scoreBoard.set_final_score(self.score)
                    self.board.game_over()
                else:
                    self.increment_score(1)
            return False

    def do_rotate(self):
        """
            Checks if the current_shape can be rotated and
            rotates if it can
            :return: If the shape was rotated
            :rtype: bool
        """

        if self.current_shape.can_rotate(self.board):
            self.current_shape.rotate(self.board)
            return True
        return False

    def key_pressed(self, event):
        """ this function is called when a key is pressed on the keyboard
            it currently just prints the value of the key

            Modify the function so that if the user presses the arrow keys
            'Left', 'Right' or 'Down', the current_shape will move in
            the appropriate direction

            if the user presses the space bar 'space', the shape will move
            down until it can no longer move and is added to the board

            if the user presses the 'Up' arrow key ,
                the shape should rotate.

        """

        key = event.keysym
        if not self.is_active:
            self.is_active = True
            self.startGame()

        elif key == 'p':
            if self.paused:
                self.paused = False
                self.scoreBoard.set_score(self.score)
            else:
                self.paused = True
                self.scoreBoard.pause_score(self.score)
            self.board.pause(self.paused)

        elif not self.paused:
            if key in self.DIRECTION:
                self.do_move(key)
            elif key == "Up":
                self.do_rotate()
            elif key == "space":
                while self.do_move("Down"):
                    self.increment_score(.05)

    def increment_score(self, inc):
        self.score += inc
        self.scoreBoard.set_score(self.score)
        return self.score
