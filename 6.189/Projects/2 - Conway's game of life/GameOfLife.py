from graphics import *
import random

############################################################
# GLOBAL VARIABLES
############################################################

BLOCK_SIZE = 20
BLOCK_OUTLINE_WIDTH = 1
BOARD_WIDTH = 50
BOARD_HEIGHT = 50

neighbor_test_blocklist = [(0, 0), (1, 1)]
toad_blocklist = [(4, 4), (3, 5), (3, 6), (5, 7), (6, 5), (6, 6)]
beacon_blocklist = [(2, 3), (2, 4), (3, 3), (3, 4), (4, 5), (4, 6), (5, 5), (5, 6)]
glider_blocklist = [(1, 2), (2, 3), (3, 1), (3, 2), (3, 3)]
#for pulsar a board size of 15x15 is needed
pulsar_blocklist = [(1, 3), (1, 4), (1, 5), (1, 9), (1, 10), (1, 11), (3, 1), (3, 6), (3, 8), (3, 13),
                    (4, 1), (4, 6), (4, 8), (4, 13), (5, 1), (5, 6), (5, 8), (5, 13), (6, 3), (6, 4), (6, 5), (6, 9),
                    (6, 10), (6, 11), (8, 3), (8, 4), (8, 5), (8, 9), (8, 10), (8, 11), (9, 1), (9, 6), (9, 8), (9, 13),
                    (10, 1), (10, 6), (10, 8), (10, 13), (11, 1), (11, 6), (11, 8), (11, 13), (13, 3), (13, 4), (13, 5),
                    (13, 9), (13, 10), (13, 11)]
# for diehard, make board at least 25x25, might need to change block size
diehard_blocklist = [(5, 7), (6, 7), (6, 8), (10, 8), (11, 8), (12, 8), (11, 6)]

############################################################
# TEST CODE (don't worry about understanding this section)
############################################################


def test_neighbors(board):
    """
    Code to test the board.get_block_neighbor function
    """
    for block in board.block_list.values():
        neighbors = board.get_block_neighbors(block)
        ncoords = [neighbor.get_coords() for neighbor in neighbors]
        if block.get_coords() == (0, 0):
            zeroneighs = [(0, 1), (1, 1), (1, 0)]
            for n in ncoords:
                if n not in zeroneighs:
                    print "Testing block at (0,0)"
                    print "Got", ncoords
                    print "Expected", zeroneighs
                    return False

            for neighbor in neighbors:
                if neighbor.get_coords() == (1, 1):
                    if not neighbor.is_live():
                        print "Testing block at (0, 0)..."
                        print "My neighbor at (1, 1) should be live; it is not."
                        print "Did you return my actual neighbors, or create new copies of them?"
                        print "FAIL: get_block_neighbors() should NOT return new Blocks!"
                        return False

        elif block.get_coords() == (1, 1):
            oneneighs = [(0, 0), (0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 2)]
            for n in ncoords:
                if n not in oneneighs:
                    print "Testing block at (1,1)"
                    print "Got", ncoords
                    print "Expected", oneneighs
                    return False
            for n in oneneighs:
                if n not in ncoords:
                    print "Testing block at (1,1)"
                    print "Got", ncoords
                    print "Expected", oneneighs
                    return False
    print "Passed neighbor test"
    return True


############################################################
# BLOCK CLASS (Read through and understand this part!)
############################################################

class Block(Rectangle):
    """ Block class:
        Implement a block for a tetris piece
        Attributes: x - type: int
                    y - type: int
        specify the position on the board
        in terms of the square grid
    """

    def __init__(self, pos, color):
        """
        pos: a Point object specifing the (x, y) square of the Block (NOT in pixels!)
        color: a string specifing the color of the block (eg 'blue' or 'purple')
        """
        self.x = pos.x
        self.y = pos.y

        p1 = Point(pos.x * BLOCK_SIZE,
                   pos.y * BLOCK_SIZE)
        p2 = Point(p1.x + BLOCK_SIZE, p1.y + BLOCK_SIZE)

        Rectangle.__init__(self, p1, p2)
        self.setWidth(BLOCK_OUTLINE_WIDTH)
        self.setFill(color)
        self.status = 'dead'
        self.new_status = 'None'

    def get_coords(self):
        return self.x, self.y

    def set_live(self, canvas):
        """
        Sets the block status to 'live' and draws it on the grid.
        Be sure to do this on the canvas!
        """
        if self.status == 'dead':
            self.status = 'live'
            self.draw(canvas)

    def set_dead(self):
        """
        Sets the block status to 'dead' and un-draws it from the grid.
        """
        if self.status == 'live':
            self.status = 'dead'
            self.undraw()

    def is_live(self):
        """
        Returns True if the block is currently 'live'. Returns False otherwise.
        """
        if self.status == 'live':
            return True
        return False

    def reset_status(self, canvas):
        """
        Sets the new_status to be the current status
        """
        if self.new_status == 'dead':
            self.set_dead()
        elif self.new_status == 'live':
            self.set_live(canvas)


###########################################################
# BOARD CLASS (Read through and understand this part!)
# Print out and turn in this section.
# Name: Tom Paulus
# Recitation: MIT-OCW
###########################################################


class Board(object):
    """ Board class: it represents the Game of Life board

        Attributes: width - type:int - width of the board in squares
                    height - type:int - height of the board in squares
                    canvas - type:CanvasFrame - where the blocks will be drawn
                    block_list - type:Dictionary - stores the blocks for a given position
    """

    def __init__(self, win, width, height):
        self.width = width
        self.height = height
        self.win = win
        # self.delay is the number of ms between each simulation. Change to be
        # shorter or longer if you wish!
        self.delay = 1000

        # create a canvas to draw the blocks on
        self.canvas = CanvasFrame(win, self.width * BLOCK_SIZE,
                                  self.height * BLOCK_SIZE)
        self.canvas.setBackground('white')

        # initialize grid lines
        for x in range(1, self.width):
            self.draw_gridline(Point(x, 0), Point(x, self.height))

        for y in range(1, self.height):
            self.draw_gridline(Point(0, y), Point(self.width, y))

        # For each square on the board, we need to initialize
        # a block and store that block in a data structure. A
        # dictionary (self.block_list) that has key:value pairs of
        # (x,y):Block will be useful here.
        self.block_list = {}

        for x in range(0, width):
            for y in range(0, height):
                square = Block(Point(x, y), 'green4')
                self.block_list[(x, y)] = square

    def draw_gridline(self, startp, endp):
        """ Parameters: startp - a Point of where to start the gridline
                        endp - a Point of where to end the gridline
            Draws two straight 1 pixel lines next to each other, to create
            a nice looking grid on the canvas.
        """
        line = Line(Point(startp.x * BLOCK_SIZE, startp.y * BLOCK_SIZE),
                    Point(endp.x * BLOCK_SIZE, endp.y * BLOCK_SIZE))
        line.draw(self.canvas)

        line = Line(Point(startp.x * BLOCK_SIZE - 1, startp.y * BLOCK_SIZE - 1),
                    Point(endp.x * BLOCK_SIZE - 1, endp.y * BLOCK_SIZE - 1))
        line.draw(self.canvas)

    def random_seed(self, percentage):
        """ Parameters: percentage - a number between 0 and 1 representing the
                                     percentage of the board to be filled with
                                     blocks
            This method activates the specified percentage of blocks randomly.
        """
        for block in self.block_list.values():
            if random.random() < percentage:
                block.set_live(self.canvas)

    def seed(self, block_coords):
        """
        Seeds the board with a certain configuration.
        Takes in a list of (x, y) tuples representing block coordinates,
        and activates the blocks corresponding to those coordinates.
        """

        for location in block_coords:
            block = self.block_list[location]
            block.set_live(self.canvas)

    def get_block_neighbors(self, block):
        """
        Given a Block object, returns a list of neighboring blocks.
        Should not return itself in the list.
        :param block: instance of Block
        """
        result = []
        x = block.x
        y = block.y

        for a in (-1, 0, 1):   # X
            for b in (-1, 0, 1):   # Y
                if a != 0 or b != 0:  # The center is where we are now
                    checkX = x + a
                    checkY = y + b
                    if checkX > -1 and checkY > -1:   # Check Top and Left Bounds
                        if checkX < BOARD_WIDTH and checkY < BOARD_HEIGHT:  # Check Bottom and Right Bounds
                            result.append(self.block_list[(checkX, checkY)])
        return result

    def simulate(self):
        """
        Executes one turn of Conways Game of Life using the rules
        listed in the handout. Best approached in a two-step strategy:

        1. Calculate the new_status of each block by looking at the
           status of its neighbors.

        2. Set blocks to 'live' if their new_status is 'live' and their
           status is 'dead'. Similarly, set blocks to 'dead' if their
           new_status is 'dead' and their status is 'live'. Then, remember
           to call reset_status(self.canvas) on each block.
        """
        for center in self.block_list:
            block = self.block_list[center]
            count = 0
            for neighbor in self.get_block_neighbors(block):
                if neighbor.status == 'live':
                    count += 1
            if count < 2 or count > 3:
                # Underpopulated or overcrowded
                block.new_status = 'dead'
            elif count == 3:
                #Reproduction
                block.new_status = 'live'
            else:
                block.new_status = None

        for center in self.block_list:
            block = self.block_list[center]
            block.reset_status(self.canvas)

    def animate(self):
        """
        Animates the Game of Life, calling "simulate"
        once every second
        """
        self.simulate()
        self.win.after(self.delay, self.animate)


        ################################################################
        # RUNNING THE SIMULATION
        ################################################################


if __name__ == '__main__':
# Initialize board
    win = Window("Conway's Game of Life")

    board = Board(win, BOARD_WIDTH, BOARD_HEIGHT)

    ## PART 1: Make sure that the board __init__ method works
    # board.random_seed(.15)

    ## PART 2: Make sure board.seed works. Comment random_seed above and uncomment
    ##  one of the seed methods below
    # board.seed(toad_blocklist)

    ## PART 3: Test that neighbors work by commenting the above and uncommenting
    ## the following two lines:
    # board.seed(neighbor_test_blocklist)
    # test_neighbors(board)


    ## PART 4: Test that simulate() works by uncommenting the next two lines:
    # board.seed(toad_blocklist)
    # win.after(2000, board.simulate)

    ## PART 5: Try animating! Comment out win.after(2000, board.simulate) above, and
    ## uncomment win.after below.
    # board.seed(glider_blocklist)
    board.random_seed(.45)
    win.after(500, board.animate)

    ## Yay, you're done! Try seeding with different blocklists (a few are provided at the top of this file!)

    win.mainloop()
