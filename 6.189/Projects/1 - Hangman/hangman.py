# Name: Tom Paulus
# Section: MIT-OCW
# 6.189 Project 1: Hangman template
# hangman_template.py

# Import statements: DO NOT delete these! DO NOT write code above this!
from random import randrange
from string import *
from hangman_lib import *

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)
# Import hangman words

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print "Loading word list from file..."
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r', 0)
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = split(line)
    print "  ", len(wordlist), "words loaded."
    print 'Enter play_hangman() to play a game of hangman!'
    return wordlist

# actually load the dictionary of words and point to it with 
# the words_dict variable so that it can be accessed from anywhere
# in the program
words_dict = load_words()


# Run get_word() within your program to generate a random secret word
# by using a line like this within your program:
# secret_word = get_word()

def get_word():
    """
    Returns a random word from the word list
    """
    word = words_dict[randrange(0, len(words_dict))]
    return word

# end of helper code
# -----------------------------------


# CONSTANTS
MAX_GUESSES = 6

# GLOBAL VARIABLES 
secret_word = 'claptrap'
letters_guessed = []


# From part 3b:
def word_guessed():
    """
    Returns True if the player has successfully guessed the word,
    and False otherwise.
    """
    global secret_word
    global letters_guessed

    for letter in secret_word:
        if not letter in letters_guessed:
            return False
    else:
        return True


def print_guessed():
    """
    Prints out the characters you have guessed in the secret word so far
    """
    global secret_word
    global letters_guessed

    word_list = []

    for letter in secret_word:
        if letter in letters_guessed:
            word_list.append(letter)
        else:
            word_list.append('-')

    return join(word_list, '')


def play_hangman():
    # Actually play the hangman game
    global secret_word
    global letters_guessed
    mistakes_made = 0
    guess = None
    full_guess = False
    done = False
    full_guess_partial = []
    secret_word = get_word()

    while True:
        print_hangman_image(mistakes_made)
        print
        print 'You have', str(MAX_GUESSES - mistakes_made), 'guesses left'
        print 'So far you have:', print_guessed()
        while True:
            guess = raw_input('>').lower()
            if len(guess) == 1:
                if not guess.isalpha():
                    print 'Please enter a letter, or a full word guess'
                elif guess in letters_guessed:
                    print 'You already guessed that letter, Try Again'
                else:
                    letters_guessed.append(guess)
                    break
            else:
                full_guess = True
                done = True
                if guess == secret_word:
                    print "Congrats, you got it! The word was", '"' + secret_word + '".'
                    break
                else:
                    mistakes_made += 1
                    for letter in guess:
                        if letter in secret_word:
                            full_guess_partial.append(letter)
                        letters_guessed.append(letter)
                    print "Sorry, that wasn't the word, but you got letters: ", join(full_guess_partial)
        if done:
            break
        if guess in secret_word and not full_guess:
            print 'Yes!'
        elif not full_guess:
            print 'Sorry,', guess, 'is not in the word'
            mistakes_made += 1

        if word_guessed() and not full_guess:
            print 'Congratulations! You guessed it!'
            break

        if mistakes_made >= MAX_GUESSES:
            print 'Sorry, you have no more guesses, the word was', '"' + secret_word + '".'
            print_hangman_image(6)
            break

play_hangman()