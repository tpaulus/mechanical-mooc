# Name: Tom Paulus
# Section: MIT-OCW
# Date: August 7 2013
# car.py

from graphics import *
from wheel import *


class Car(object):
    def __init__(self, point1, radius1, point2, radius2, car_height):
        #Points
        self.p1 = point1
        self.r1 = radius1
        self.p2 = point2
        self.r2 = radius2
        self.car_height = car_height
        # Shapes
        self.body_rectangle = Rectangle(self.p1, Point(self.p2.x, int(self.p2.y) - int(self.car_height)))
        self.left_tire = Wheel(self.p1, int(self.r1 * 0.60), self.r1)
        self.right_tire = Wheel(self.p2, int(self.r2 * 0.60), self.r2)

    def draw(self, win):
        self.body_rectangle.draw(win)
        self.left_tire.draw(win)
        self.right_tire.draw(win)

    def set_color(self, tires, wheels, body):
        self.left_tire.set_color(wheels, tires)
        self.right_tire.set_color(wheels, tires)
        self.body_rectangle.setFill(body)

    def animate(self, part, win, dx, dy, n):
        """
        :type win: GraphWin object
        """
        if n > 0:
            part.move(dx, dy)
            win.after(100, self.animate, part, win, dx, dy, n - 1)

    def drive(self, window, dx, dy, steps):
        self.animate(self.left_tire, window, dx, dy, steps)
        self.animate(self.right_tire, window, dx, dy, steps)
        self.animate(self.body_rectangle, window, dx, dy, steps)

new_win = GraphWin("Car", 700, 300)

car1 = Car(Point(50, 50), 15, Point(100, 50), 15, 40)
car1.draw(new_win)
car1.set_color('black', 'grey', 'pink')

car1.drive(new_win, 1, 0, 400)

new_win.mainloop()