# Name: Tom Paulus
# Section: MIT-OCW
# Date: August 7 2013
# tetrominoes.py

from graphics import *


class Block(Rectangle):
    def __init__(self, center_point, block_color):
        self.center = center_point
        self.color = block_color

        top_left = Point(self.center.x * 30 - 15, self.center.y * 30 - 15)
        bottom_right = Point(self.center.x * 30 + 15, self.center.y * 30 + 15)
        Rectangle.__init__(self, top_left, bottom_right)
        self.setFill(self.color)


class Shape:
    def __init__(self, point_list, color):
        self.points = point_list
        self.color = color
        self.block_list = []

        for b in point_list:
            block = Block(b, color)
            self.block_list.append(block)

    def draw(self, win):
        for b in self.block_list:
            b.draw(win)


class I_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 2, center.y),
                  Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "blue")


class J_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y),
                  Point(center.x + 1, center.y + 1)]
        Shape.__init__(self, coords, "orange")


class L_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y + 1),
                  Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "turquoise2")


class O_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x - 1, center.y + 1),
                  Point(center.x, center.y),
                  Point(center.x, center.y + 1)]
        Shape.__init__(self, coords, "red")


class S_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y + 1),
                  Point(center.x, center.y + 1),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "green")


class T_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x, center.y + 1),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "yellow")


class Z_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x, center.y + 1),
                  Point(center.x + 1, center.y + 1)]
        Shape.__init__(self, coords, "deep pink")


if __name__ == '__main__':
    win = GraphWin("Tetrominoes", 900, 150)

    # block = Block(Point(1, 1), 'red')
    # block.draw(win)

    # I = I_shape(Point(3, 1))
    # I.draw(win)

    tetrominoes = [I_shape, J_shape, L_shape, O_shape, S_shape, T_shape, Z_shape]
    x = 3
    for tetromino in tetrominoes:
        shape = tetromino(Point(x, 1))
        shape.draw(win)
        x += 4

    win.mainloop()